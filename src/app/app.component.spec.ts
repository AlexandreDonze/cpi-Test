import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { MaterialModule } from './app.module';
import { AuthenticationHttpService, AuthenticationHttpFacade } from './services/authentication-http.service';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';



class MockRouter {

	public navigate(param: any): void {
	}
}


class MockAuthenticationHttpService implements AuthenticationHttpFacade {

	public signin(email: string, password: string) {
	}

	public signout(): void {
	}

	public isLogged(): boolean {
		return true;
	}
}


describe('AppComponent', () => {
	let app: any;
	let fixture: ComponentFixture<AppComponent>;
	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [
				RouterTestingModule,
				FormsModule,
				ReactiveFormsModule,
				BrowserAnimationsModule,
				MaterialModule,
			],
			declarations: [
				AppComponent
			],
			providers: [
				AuthenticationHttpService
			]
		})
			.overrideComponent(AppComponent, {
				set: {
					providers: [
						{ provide: Router, useClass: MockRouter },
						{ provide: AuthenticationHttpService, useClass: MockAuthenticationHttpService }
					]
				}
			})
			.compileComponents();

		fixture = TestBed.createComponent(AppComponent);
		app = fixture.debugElement.componentInstance;

	}));

	it('should create the app', async(() => {
		expect(app).toBeTruthy();
	}));

	it('signout', async(() => {
		spyOn(app.router, 'navigate').and.callFake((arg) => {
			expect(arg).toEqual(['login']);
		});
		spyOn(app.authenticationHttpService, 'signout');
		app.signout();
		expect(app.authenticationHttpService.signout).toHaveBeenCalledTimes(1);
	}));

	it('isLogged', async(() => {
		spyOn(app.authenticationHttpService, 'isLogged').and.returnValue(true);
		const islogged: boolean = app.isLogged();
		expect(islogged).toEqual(true);
		expect(app.authenticationHttpService.isLogged).toHaveBeenCalledTimes(1);
	}));

	it('click sign out button', () => {
		spyOn(app, 'signout');

		const element = fixture.nativeElement;
		const button = element.querySelector('button');
		button.dispatchEvent(new Event('click'));

		expect(app.signout).toHaveBeenCalled();
	});
});


