import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { BooksComponent } from './books/books.component';
import { AuthGuard } from './guard/auth.guard';

const ROUTES: Routes = [
	{ path: 'login', component: LoginComponent },
	{ path: 'books', component: BooksComponent, canActivate: [AuthGuard] },
	{ path: '', redirectTo: 'login', pathMatch: 'full'  },
	{ path: '**', redirectTo: 'login', pathMatch: 'full' }
];

@NgModule({
	imports: [RouterModule.forRoot(ROUTES)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
