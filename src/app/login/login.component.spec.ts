import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { MaterialModule } from '../app.module';
import { AuthenticationHttpService, AuthenticationHttpFacade } from '../services/authentication-http.service';
import { LoginComponent } from './login.component';

class MockRouter {
	public navigate(param: any): void {
	}
}

class MockAuthenticationHttpService implements AuthenticationHttpFacade {
	public async signin(email: string, password: string) {
		return Promise.resolve();
	}

	public signout(): void {
	}

	public isLogged(): boolean {
		return true;
	}
}

describe('LoginComponent', () => {
	let component: LoginComponent | any;
	let fixture: ComponentFixture<LoginComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [
				FormsModule,
				ReactiveFormsModule,
				BrowserAnimationsModule,
				MaterialModule,
			],
			declarations: [LoginComponent]
		})
			.overrideComponent(LoginComponent, {
				set: {
					providers: [
						{ provide: Router, useClass: MockRouter },
						{ provide: AuthenticationHttpService, useClass: MockAuthenticationHttpService }
					]
				}
			})
			.compileComponents();

		fixture = TestBed.createComponent(LoginComponent);
		component = fixture.componentInstance;
	}));

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('signin Ok', (done) => {
		spyOn(component.authenticationHttpService, 'signin').and.callFake(() => {
			return Promise.resolve();
		});

		component.signin()
			.then((status) => {
				expect(status).toBe(true);
				done();
			});
	});

	it('signin Nok', (done) => {
		spyOn(component.authenticationHttpService, 'signin').and.callFake(() => {
			return Promise.reject(null);
		});

		component.signin()
			.then((status) => {
				expect(status).toBe(false);
				done();
			});
	});

	it('click on sign in button', () => {
		spyOn(component, 'signin');

		const element = fixture.nativeElement;
		const button = element.querySelector('form');
		button.dispatchEvent(new Event('ngSubmit'));

		fixture.detectChanges();

		expect(component.signin).toHaveBeenCalled();
	});
});
