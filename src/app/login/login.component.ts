import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationHttpService } from '../services/authentication-http.service';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})
export class LoginComponent {

	public emailAddress: string;
	public password: string;
	public wrongInput: boolean = false;

	public signinPending: boolean;

	constructor(
		private router: Router,
		private authenticationHttpService: AuthenticationHttpService) {
	}

	public async signin() {
		try {
			this.signinPending = true;
			const response: any = await this.authenticationHttpService.signin(this.emailAddress, this.password);
			this.signinPending = false;
			return Promise.resolve(true);
		} catch (error) {
			this.wrongInput = true;
			this.signinPending = false;
			return Promise.resolve(false);
		}
	}

}
