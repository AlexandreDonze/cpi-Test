import { TestBed, inject } from '@angular/core/testing';
import { Router } from '@angular/router';

import { AuthGuard } from './auth.guard';
import { AuthenticationHttpService, AuthenticationHttpFacade } from '../services/authentication-http.service';


describe('AuthGuard', () => {
	const mockRouter = jasmine.createSpyObj('Router', ['navigate']);
	const mockAuthenticationHttpService = jasmine.createSpyObj('AuthenticationHttpService', ['isLogged']);

	beforeEach(() => TestBed.configureTestingModule({
		providers: [
			{ provide: Router, useValue: mockRouter },
			{ provide: AuthenticationHttpService, useValue: mockAuthenticationHttpService },
			AuthGuard
		]
	}));

	it('route can active', () => {
		mockAuthenticationHttpService.isLogged.and.returnValue(false);
		const service = TestBed.get(AuthGuard);
		const result: boolean = service.canActivate(null, null);
		expect(result).toBe(false);
		expect(mockRouter.navigate).toHaveBeenCalledWith(['login']);
	});

	it('route cannot active', () => {
		mockAuthenticationHttpService.isLogged.and.returnValue(true);
		const service = TestBed.get(AuthGuard);
		const result: boolean = service.canActivate(null, null);
		expect(result).toBe(true);
	});
});

