import { Injectable } from '@angular/core';
import { AuthenticationHttpService } from '../services/authentication-http.service';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AuthGuard implements CanActivate {

	constructor(
		private router: Router,
		private authenticationHttpService: AuthenticationHttpService) {
	}

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
		if (!this.authenticationHttpService.isLogged()) {
			this.router.navigate(['login']);
			return false;
		} else {
			return true;
		}
	}
}
