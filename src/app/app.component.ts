import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationHttpService } from './services/authentication-http.service';


@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

	constructor(
		private router: Router,
		private authenticationHttpService: AuthenticationHttpService) {
	}

	ngOnInit() {
		this.authenticationHttpService.getSubscribtion().subscribe((isloggged) => {
			if (isloggged) {
				this.router.navigate(['books']);
			} else {
				this.router.navigate(['login']);
			}
		});
	}

	public signout(): void {
		this.authenticationHttpService.signout();
	}

	public isLogged(): boolean {
		return this.authenticationHttpService.isLogged();
	}
}
