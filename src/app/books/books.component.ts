import { Component, OnInit } from '@angular/core';
import { BooksHttpService, Book, BookPage } from '../services/books-http.service';

@Component({
	selector: 'app-books',
	templateUrl: './books.component.html',
	styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

	public page: BookPage = new BookPage(0, 0, 0, 0, new Array<Book>());
	public booksPendding: boolean = false;

	constructor(private booksHttpService: BooksHttpService) {
	}

	ngOnInit() {
		this.loadPage(1);
	}

	private async loadPage(pageNumber: number) {
		try {
			this.booksPendding = true;
			this.page = await this.booksHttpService.getPage(pageNumber);
			this.booksPendding = false;
			return Promise.resolve();
		} catch (error) {
			this.booksPendding = false;
			return Promise.reject(error);
		}
	}

	public async prevPage() {
		try {
			await this.loadPage(this.page.page - 1);
			return Promise.resolve(true);
		} catch (error) {
			return Promise.resolve(false);
		}
	}

	public async nextPage() {
		try {
			await this.loadPage(this.page.page + 1);
			return Promise.resolve(true);
		} catch (error) {
			return Promise.resolve(false);
		}
	}

}
