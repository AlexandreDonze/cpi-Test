import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MaterialModule } from '../app.module';

import { BooksComponent } from './books.component';
import { BooksHttpService, BooksHttpFacade, BookPage, Book } from '../services/books-http.service';

class MockBooksHttpService implements BooksHttpFacade {
	getPage(pageNumber: number) {
		return new BookPage(2, 0, 0, 4, []);
	}
}

describe('BooksComponent #1', () => {
	let component: any;
	let fixture: ComponentFixture<BooksComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [
				FormsModule,
				ReactiveFormsModule,
				BrowserAnimationsModule,
				MaterialModule,
			],
			declarations: [BooksComponent],
			providers: [BooksHttpService]
		})
			.overrideComponent(BooksComponent, {
				set: {
					providers: [
						{ provide: BooksHttpService, useClass: MockBooksHttpService }
					]
				}
			})
			.compileComponents();

		fixture = TestBed.createComponent(BooksComponent);
		component = fixture.componentInstance;
	}));

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('click next page button', () => {
		spyOn(component, 'nextPage');

		const element = fixture.nativeElement;
		const button = element.querySelector('#nextPage');
		button.dispatchEvent(new Event('click'));

		expect(component.nextPage).toHaveBeenCalled();
	});

	it('click prev page button', () => {
		spyOn(component, 'prevPage');

		const element = fixture.nativeElement;
		const button = element.querySelector('#prevPage');
		button.dispatchEvent(new Event('click'));

		expect(component.prevPage).toHaveBeenCalled();
	});

	it('page counter', () => {

		spyOn(component, 'loadPage').and.callFake((pageNumber) => {
			component.page = new BookPage(1, 0, 0, 4, []);
		});

		fixture.detectChanges();

		const element = fixture.nativeElement;
		expect(element.querySelector('label').textContent).toBe('1 / 4');
	});

	it('filled list', () => {

		spyOn(component, 'loadPage').and.callFake((pageNumber) => {
			component.page = new BookPage(1, 0, 0, 4, [
				new Book(0, 'name#1', 2001, 'color#1', 'pantone#1'),
				new Book(0, 'name#2', 2002, 'color#2', 'pantone#2'),
				new Book(0, 'name#3', 2003, 'color#3', 'pantone#3'),
			]);
		});

		fixture.detectChanges();

		const element = fixture.nativeElement;
		const titles = element.querySelectorAll('mat-panel-title');
		let i: number = 0;
		for (const title of titles) {
			++i;
			expect(title.textContent).toContain('name#' + i);
		}

		const descriptions = element.querySelectorAll('mat-panel-description');
		i = 0;
		for (const description of descriptions) {
			++i;
			expect(description.textContent).toContain(2000 + i);
		}

		const pantones = element.querySelectorAll('#pantone');
		i = 0;
		for (const pantone of pantones) {
			++i;
			expect(pantone.textContent).toContain('pantone#' + i);
		}
	});

});

describe('BooksComponent #2', () => {
	let component: any;
	let fixture: ComponentFixture<BooksComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [
				FormsModule,
				ReactiveFormsModule,
				BrowserAnimationsModule,
				MaterialModule,
			],
			declarations: [BooksComponent],
			providers: [BooksHttpService]
		})
			.overrideComponent(BooksComponent, {
				set: {
					providers: [
						{ provide: BooksHttpService, useClass: MockBooksHttpService }
					]
				}
			})
			.compileComponents();

		fixture = TestBed.createComponent(BooksComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	}));

	it('next page', (done) => {
		const page = new BookPage(2, 0, 0, 0, []);

		spyOn(component.booksHttpService, 'getPage').and.callFake((pageNumber) => {
			expect(pageNumber).toBe(3);
			return Promise.resolve(page);
		});

		component.nextPage()
			.then((result) => {
				expect(result).toBe(true);
				expect(component.page.page).toBe(2);
				done();
			});
	});

	it('prev page', (done) => {
		const page = new BookPage(2, 0, 0, 0, []);
		spyOn(component.booksHttpService, 'getPage').and.callFake((pageNumber) => {
			expect(pageNumber).toBe(1);
			return Promise.resolve(page);
		});

		component.prevPage()
			.then((result) => {
				expect(result).toBe(true);
				expect(component.page.page).toBe(2);
				done();
			});
	});

});
