import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

const URL_LOGIN: string = 'https://reqres.in/api/login';

export interface AuthenticationHttpFacade {
	signin(email: string, password: string);
	signout(): void;
	isLogged(): boolean;
}

@Injectable()
export class AuthenticationHttpService implements AuthenticationHttpFacade {

	private status: Subject<boolean> = new Subject<boolean>();
	private islogged: boolean = false;

	constructor(private http: Http) {
	}

	public getSubscribtion(): Observable<boolean> {
		return this.status;
	}

	private setSignin(): void {
		this.islogged = true;
		this.status.next(true);
	}

	private setSignout(): void {
		this.islogged = false;
		this.status.next(false);
	}

	public async signin(email: string, password: string) {
		try {
			const response: any = await this.http.post(URL_LOGIN, { email: email, password: password }).toPromise();
			const body: any = JSON.parse(response._body);
			if (response.status === 200 && body.token === 'QpwL5tke4Pnpja7X') {
				this.setSignin();
				return Promise.resolve(body);
			}
			return Promise.reject(response.status);
		} catch (error) {
			return Promise.reject(error);
		}
	}

	public signout(): void {
		this.setSignout();
	}

	public isLogged(): boolean {
		return this.islogged;
	}
}
