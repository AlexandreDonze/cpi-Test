import { TestBed, inject } from '@angular/core/testing';
import { HttpModule, XHRBackend, Response, ResponseOptions } from '@angular/http';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { BooksHttpService, BookPage, Book } from './books-http.service';

describe('BooksHttpService', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [HttpModule],
			providers: [BooksHttpService,
				{ provide: XHRBackend, useClass: MockBackend }
			]
		});
	});

	it('should be created', inject([BooksHttpService], (service: BooksHttpService) => {
		expect(service).toBeTruthy();
	}));

	it('get page', (done) => {
		inject([XHRBackend, BooksHttpService], (mockBackend: MockBackend, service: BooksHttpService) => {
			const pageNumber: number = 1;
			const body = '{"page":2,"per_page":3,"total":12,"total_pages":4,"data":[{"id":4,"name":"aqua sky","year":2003,"color":"#7BC4C4","pantone_value":"14-4811"},{"id":5,"name":"tigerlily","year":2004,"color":"#E2583E","pantone_value":"17-1456"},{"id":6,"name":"blue turquoise","year":2005,"color":"#53B0AE","pantone_value":"15-5217"}]}';
			const status = 200;
			mockBackend.connections.subscribe((connection: MockConnection) => {
				connection.mockRespond(new Response(new ResponseOptions({ body, status })));
			});

			service.getPage(pageNumber)
				.then(response => {
					expect(response).not.toBeNull();
					const page: BookPage = <BookPage>response;
					expect(page).toEqual(JSON.parse(body));
					done();
				});
		})();
	});
});
