import { TestBed, inject } from '@angular/core/testing';
import { HttpModule, XHRBackend, Response, ResponseOptions } from '@angular/http';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { AuthenticationHttpService } from './authentication-http.service';

describe('AuthenticationHttpService', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [HttpModule],
			providers: [AuthenticationHttpService,
				{ provide: XHRBackend, useClass: MockBackend }
			]
		});
	});

	it('should be created', inject([AuthenticationHttpService], (service: AuthenticationHttpService) => {
		expect(service).toBeTruthy();
	}));

	it('signin', (done) => {
		inject([XHRBackend, AuthenticationHttpService], (mockBackend: MockBackend, service: AuthenticationHttpService) => {
			const email: string = '';
			const password: string = '';
			const body = '{ "token": "QpwL5tke4Pnpja7X" }';
			const status = 200;
			mockBackend.connections.subscribe((connection: MockConnection) => {
				connection.mockRespond(new Response(new ResponseOptions({ body, status })));
			});

			service.signin(email, password)
				.then(response => {
					expect(response).not.toBeNull();
					expect(response).toEqual(JSON.parse(body));
					done();
				});
		})();
	});
});
