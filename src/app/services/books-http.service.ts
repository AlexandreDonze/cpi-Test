import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

const URL_BOOKS: string = 'https://reqres.in/api/books?page=';

export class Book {

	public constructor(
		public id: number,
		public name: string,
		public year: number,
		public color: string,
		public pantone_value: string
	) {
	}
}

export class BookPage {

	public constructor(
		public page: number,
		public per_page: number,
		public total: number,
		public total_pages: number,
		public data: Array<Book>) {
	}
}

export interface BooksHttpFacade {
	getPage(pageNumber: number);
}

@Injectable()
export class BooksHttpService implements BooksHttpFacade {

	constructor(private http: Http) {
	}

	public async getPage(pageNumber: number) {
		try {
			const response: any = await this.http.get(URL_BOOKS + pageNumber).toPromise();
			const body = JSON.parse(response._body);
			if (response.status === 200) {
				return Promise.resolve(body);
			}
			return Promise.reject(response.status);
		} catch (error) {
			return Promise.reject(error);
		}
	}

}
