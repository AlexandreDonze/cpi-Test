import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { BooksComponent } from './books/books.component';

import { AuthenticationHttpService } from './services/authentication-http.service';
import { BooksHttpService } from './services/books-http.service';
import { AuthGuard } from './guard/auth.guard';

import {
	MatButtonModule,
	MatIconModule,
	MatListModule,
	MatToolbarModule,
	MatGridListModule,
	MatInputModule,
	MatExpansionModule
} from '@angular/material';

import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
	exports: [
		MatButtonModule,
		MatIconModule,
		MatListModule,
		MatToolbarModule,
		MatGridListModule,
		MatInputModule,
		MatExpansionModule,
		FlexLayoutModule
	]
})
export class MaterialModule { }

@NgModule({
	declarations: [
		AppComponent,
		LoginComponent,
		BooksComponent
	],
	imports: [
		BrowserModule,
		MaterialModule,
		AppRoutingModule,
		HttpModule,
		FormsModule,
		BrowserAnimationsModule
	],
	providers: [
		AuthenticationHttpService,
		BooksHttpService,
		AuthGuard
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
